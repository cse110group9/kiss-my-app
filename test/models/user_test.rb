require 'test_helper'

class TestUsers < ActiveSupport::TestCase
  test "a user must have a password >= 8 chars" do
    u = new_user
    assert u.valid?, u.errors.full_messages
  end

  test "password and confirmation must match" do
    u = new_user
    u.password_confirmation = "blah"

    assert u.invalid?
  end

  test "invalid if password is < 8 chars" do
    u = new_user
    u.password = u.password_confirmation = "short"

    assert u.invalid?
  end

  test "email must be unique" do
    u = new_user
    assert u.save

    u2 = new_user
    assert !u2.save
  end

  test "factory works" do
    u = create(:user)
    assert u.valid?
  end

  test "packages can be cloned to a user" do
    generate_user_with_package!

    assert 1, @u.subscriptions.length
    assert 0, @u.custom_packages.length
    sub = @u.subscriptions.first

    sub.clone_package!

    @u.reload
    assert 1, @u.subscriptions.length
    assert 1, @u.custom_packages.length
  end

private
  def new_user
    User.new({
      name: "Tester",
      address: "123 Blah Ln.",
      email: "test@email.com",
      password: "12345678",
      password_confirmation: "12345678",
      account_type: :retail
    })
  end
end
