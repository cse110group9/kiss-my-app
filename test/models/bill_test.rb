require 'test_helper'

class TestBills < ActiveSupport::TestCase
  test "bills have a total cost" do
    bill = Bill.create
    bill.bill_items.create(item_type: :service, price: 10, name: "Test Item 1")
    bill.bill_items.create(item_type: :service, price: 5, name: "Test Item 2")

    assert_equal bill.cost, 15
  end

  test "bills are created for users" do
    generate_user_with_package!

    # generate a bill
    bill = Bill.generate_for(@u)

    # should only be the price of the package
    total_bill_cost = @p.price
    assert_equal bill.cost, total_bill_cost
  end

  test "bill items are nested" do
    generate_user_with_package!

    bill = Bill.generate_for(@u)

    # bill items for: package, and its two services
    assert_equal 3, bill.bill_items.length
    assert_equal 1, bill.toplevel_bill_items.length
  end
end
