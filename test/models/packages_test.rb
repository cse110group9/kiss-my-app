require 'test_helper'

class TestPackages < ActiveSupport::TestCase
  test "packages require a name" do
    assert Package.new(name: nil).invalid?
  end

  test "factory is valid" do
    assert create(:package).valid?
    assert create(:package).valid?
  end
end
