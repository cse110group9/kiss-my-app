ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest'
require 'minitest/autorun'
require 'minitest/pride'
require 'database_cleaner'
require 'factory_girl'
require 'pry'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  include FactoryGirl::Syntax::Methods
end

class ActiveSupport::TestCase
  # include FactoryGirl::Syntax::Methods

  setup do
    DatabaseCleaner.start
  end

  teardown do
    DatabaseCleaner.clean
  end

  def generate_user_with_package!
    # create package w/ services
    @p = create(:package)
    @ss = [create(:service), create(:service)]
    @p.services << @ss

    # add that package to the user
    @u = create(:user)
    @u.packages << @p
    @u
  end
end

FactoryGirl.define do
  factory :user do
    name "Ima User"
    address "123 what of it st"
    sequence(:email) { |n| "test#{n}@test.test" }

    password "asdfasdf"
    password_confirmation "asdfasdf"
    account_type :retail
  end

  factory :package do
    sequence(:name) { |n| "Package #{n}"}
    num_early_term_days 10
    price { Random.new.rand(10..100) }
  end

  factory :service do
    sequence(:name) { |n| "Service #{n}"}
    num_early_term_days 10
    price { Random.new.rand(10..100) }
    description "Description of a service, here"
  end
end

DatabaseCleaner.cleaning do
  FactoryGirl.lint
end
