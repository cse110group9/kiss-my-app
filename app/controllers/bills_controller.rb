class BillsController < ApplicationController
  def pay
    @bill = Bill.find(params[:bill_id])
    @bill.paid_on = Time.now
    @bill.save
    flash[:notice] = "Paid bill"
    redirect_to root_authed_path
  end
end
