class UsersController < AuthedAppController

  def show
    @managed_user = User.find(params[:id])
  end

  def generate_bill
    @managed_user = User.find(params[:user_id])
    @managed_user.generate_bill

    redirect_to user_path(@managed_user.id)
  end
end
