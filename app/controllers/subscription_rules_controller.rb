class SubscriptionRulesController < AuthedAppController
  def index
    @srules = SubscriptionRule.all
  end

  def create
    @srule = SubscriptionRule.create(subscription_rules_params)
    @srules = SubscriptionRule.all
    if @srule.valid?
      @srule = nil
    end
    render "index"
  end

  def update
    @srule = SubscriptionRule.find(params[:id])
    @srule.update_attributes(subscription_rules_params)
    if params[:commit] == "Destroy"
      @srule.destroy
    end

    redirect_to subscription_rules_path
  end

  def subscription_rules_params
    params.require(:subscription_rule).permit(:rule_string)
  end
end
