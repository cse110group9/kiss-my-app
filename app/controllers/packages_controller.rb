class PackagesController < AuthedAppController
  # before_action :require_is_marketing
  before_action :set_packages, only: [:index, :create, :update]

  def index
  end

  # def new
  # end

  def create
    @package = Package.new(package_params)
    @package.save

    render "index"
  end

  # def edit
  # end

  def update
    @package = Package.find(params[:id])
    success = @package.update_attributes(package_params)

    if success
      flash[:notice] = "Updated Package"
    else
      flash[:alert] = "Failed to update package: #{@package.errors.full_messages.join(", ")}"
    end

    if params[:redirect_to]
      redirect_to params[:redirect_to]
    else
      redirect_to packages_path
    end
  end

  def destroy
  end

protected
  def set_packages
    @packages = Package.all
  end

  def package_params
    params.require(:package).permit(:name, :price, :early_term_fee, service_ids: [[:inc, :id]])
  end
end
