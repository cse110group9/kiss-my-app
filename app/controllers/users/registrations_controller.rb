class Users::RegistrationsController < Devise::RegistrationsController
  before_action :set_user, only: [:edit, :update, :destroy]
  before_action :configure_permitted_parameters, only: [:create, :update]
  before_action :filter_acceptable_user_types, only: [:create, :update]

  layout "layouts/dashboard", except: [:new, :create]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

protected
  def configure_permitted_parameters
    [:name, :address, :account_type, :bill_threshold].each do |arg|
      devise_parameter_sanitizer.for(:sign_up)        << arg
      devise_parameter_sanitizer.for(:account_update) << arg
    end
  end

  def filter_acceptable_user_types
    valid_types = [:retail, :commercial]

    if params[:user] && params[:user][:account_type]
      unless valid_types.include? params[:user][:account_type].to_sym
        params[:user][:account_type] = nil
      end
    end
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
