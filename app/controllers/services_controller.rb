class ServicesController < AuthedAppController
  before_action :require_is_marketing
  # before_action :set_service, except: [:index, :new, :create]
  before_action :set_service, except: [:index, :create]
  before_action :set_all_services, except: []

  def index
  end

  def create
    @service = Service.new(service_params)
    @service.save
    render :index
  end

  def update
    @service.update_attributes(service_params)
    redirect_to services_path
  end

  def destroy
    @service = Service.find(params[:id])
    @service.destroy
    redirect_to services_path
  end

  def disable
    @service.disabled!
    redirect_to services_path
  end

  def enable
    @service.enabled!
    redirect_to services_path
  end

protected
  def set_service
    @service = Service.find(params[:id] || params[:service_id])
  end

  def service_params
    params.require(:service).permit(:name, :price, :description, :early_term_fee)
  end

  def set_all_services
    @services = Service.all
  end
end
