class SubscriptionsController < AuthedAppController
  before_action :set_subscribables, only: [:index, :create, :destroy]

  def index
  end

  def create
    @subscription = Subscription.create(subscription_params) do |s|
      s.user = @user
    end

    flash[:notice] = "Subscribed to #{@subscription.subscribable.name}"

    render :index
  end

  def destroy
    @subscription = Subscription.find(params[:id])
    flash[:notice] = "Unsubscribed from #{@subscription.subscribable.name}"
    @subscription.destroy
    # redirect_to root_authed_path
    redirect_to (request.env['HTTP_REFERER']) || root_authed_path
  end

  def create_for
    @managed_user = User.find(params[:user_id])
    @subscription = Subscription.create(subscription_params) do |s|
      s.user = @managed_user
    end

    if @subscription.valid?
      flash[:notice] = "Subscribed to #{@subscription.subscribable.name}"
    else
      flash[:alert] = "Error: #{@subscription.errors.full_messages.join(", ")}"
    end
    redirect_to (request.env['HTTP_REFERER']) || root_authed_path
  end

  def clone_package
    @managed_user = User.find(params[:user_id])
    @subscription = Subscription.find(params[:subscription_id])

    pkg = @subscription.subscribable
    pkg = pkg.is_a? Package
    if pkg
      @subscription.clone_package!
      flash[:notice] = "Created custom package"
    else
      flash[:alert] = "Error: Selected subscription wasn't a package"
    end

    redirect_to @managed_user
  end

protected
  def set_subscribables
    @services = Service.where.not(id: @user.services)
    @packages = Package.where.not(id: @user.packages)
  end

  def subscription_params
    params.require(:subscription).permit(:subscribable_str, :term_length)
  end
end
