class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

protected
  def set_user
    @user = current_user

    if !@user
      flash[:notice] = "Please log in first"
      redirect_to root_unauthed_path
    end
  end

  def require_is_marketing
    if !@user || !@user.marketing?
      if !@user
        redirect_to root_unauthed_path
      else
        flash[:alert] = "You must be a marketing agent to access this page"
        redirect_to root_authed_path
      end
    end
  end

end
