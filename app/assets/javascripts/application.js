// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require_tree .
$(function() {
  var pass_input = $(".registration-new input[type=password]").first();
  if(pass_input.length) {
    var strength_checks = [
      // At least one lowercase
      /[a-z]+/,
      // at least one uppercase
      /[A-Z]+/,
      // at least one number
      /[0-9]+/,
      // at least one non alphanumeric
      /[^a-zA-Z0-9]/
    ];

    var conf_input = $(".registration-new input[type=password]").last();
    var strength_msg = $(".registration-new .strength");
    var conf_msg = $(".registration-new .confirmation");

    // Add a "strength" indicator
    pass_input.on("keyup", function() {
      strength_msg.removeClass("weak");
      strength_msg.removeClass("medium");
      strength_msg.removeClass("strong");

      var val = pass_input.val();
      if(val.length < 8) {
        strength_msg.text("Password too short");
        return;
      }

      var passed = 0;
      strength_checks.forEach(function(check) {
        if(check.test(val)) { passed += 1; }
      });

      switch(passed) {
      case 0:
      case 1:
        strength_msg.addClass("weak");
        strength_msg.text("Weak password");
        break;
      case 2:
        strength_msg.addClass("medium");
        strength_msg.text("Medium strength password");
        break;

      default:
        strength_msg.addClass("strong");
        strength_msg.text("Strong password");
        break;
      }
    });

    var checkconf = function() {
      if(pass_input.val().length < 8) {
        conf_msg.text("");
        return;
      }

      if(conf_input.val() != pass_input.val()) {
        conf_msg.text("Password and Password confirmation do not match");
      }
      else {
        conf_msg.text("");
      }
    }

    pass_input.on("keyup", checkconf);
    conf_input.on("keyup", checkconf);
  }
});
