class Subscription < ActiveRecord::Base
  belongs_to :user
  belongs_to :subscribable, polymorphic: true

  validates_presence_of :user, :subscribable_id, :subscribable_type, :term_date
  validates_uniqueness_of :user, scope: [:subscribable_id, :subscribable_type]

  validates_numericality_of :num_early_term_days, if: :num_early_term_days?
  validates_numericality_of :early_term_fee,      if: :early_term_fee?

  def clone_package!
    if subscribable.is_a? Package
      if subscribable.specific_to
        raise "Internal error: can't clone a package that is already cloned"
      end
      self.subscribable = self.subscribable.clone_to user
      self.save!
    else
      raise "Internal error: subscribable isn't a package"
    end
  end

  def subscribable_str= sstr
    klass, id = sstr.to_s.split("_")
    if klass && id
      self.subscribable_type = klass
      self.subscribable_id   = id
    else
      self.subscribable_id   = klass
    end
  end

  def subscribable_str
    if subscribable_id && subscribable_type
      "#{subscribable_type}_#{subscribable_id}"
    else
      nil
    end
  end

  def term_length= term_str
    self.term_date = case term_str
    when "1week" then 1.week.from_now
    when "2weeks" then 2.weeks.from_now
    when "1month" then 1.month.from_now
    when "3months" then 3.months.from_now
    else 1.month.from_now
    end
  end
  def term_length
    "1month"
  end

  def pretty_term_date
    if term_date
      term_date.to_time.strftime('%B %e')
    else
      "Never"
    end
  end

  def num_early_term_days
    super || subscribable.num_early_term_days
  end

  def early_term_fee
    super || subscribable.early_term_fee
  end

  # check if an early termination fee applies
  before_destroy do
    s = subscribable
    if created_at > self.num_early_term_days.minutes.ago
      bill = Bill.create!(user: user)
      bill.bill_items.create!({
        item_type: :early_term,
        name: "Early Termination Fee for #{s.name}",
        price: early_term_fee
      })
    end
  end

  # clean up any specific-to packages
  before_destroy do
    s = subscribable
    if s.is_a?(Package) && s.specific_to
      s.destroy
    end
  end

  before_validation do
    self.term_date ||= 1.month.from_now
  end

  # Run custom rules on subscriptions before validating/saving the sub
  before_validation do
    SubscriptionRule.all.each do |rule|
      begin
        eval(rule.rule_string)
      rescue
        Rails.log.error("Failed to run rule ID: #{rule.id}")
      end
    end
    true
  end
end
