class Bill < ActiveRecord::Base
  has_many :bill_items

  has_many :toplevel_bill_items,
    -> { toplevel },
    class_name: "BillItem"

  scope :unpaid, -> { where(paid_on: nil) }

  belongs_to :user

  def self.generate_for user
    bill = Bill.new(user: user)
    bill.save!

    user.subscriptions.each do |sub|
      item = bill.bill_items.build
      subscribable = sub.subscribable

      item.price = subscribable.price
      item.name = subscribable.name

      if subscribable.is_a? Service
        item.item_type = :service
      elsif subscribable.is_a? Package
        item.item_type = :package
      else
        raise "invalid subscribable type"
      end

      item.save!

      # if item is a package, save its sub-items as well
      if subscribable.is_a? Package
        subscribable.services.each do |service|
          subitem = bill.bill_items.build({
            item_type: :service,
            price: service.price,
            name: service.name,
            package_bill_item: item
          })

          item.service_bill_items << subitem
          subitem.save!
        end
      end
    end

    bill
  end

  def pretty_date
    created_at.to_time.strftime('%B %e at %l:%M %p')
  end

  def cost
    toplevel_bill_items.map(&:price).reduce(0, &:+)
  end

private
  after_create do
    if user && user.bill_threshold && user.total_debt >= user.bill_threshold
      # should send a notification to the user
      BillThreshMailer.alert(user).deliver
    end
    true
  end
end
