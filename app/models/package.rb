class Package < ActiveRecord::Base
  has_many :packaged_services
  has_many :services, through: :packaged_services
  accepts_nested_attributes_for :packaged_services

  validates_presence_of :name, :price, :num_early_term_days
  validates_numericality_of :price, :num_early_term_days

  # can users sign up for this pacakge?
  enum status: [:active, :discontinued]

  # when a package is cloned, it belongs to a specific user
  # (it was based off of a template package)
  belongs_to :specific_to, class_name: "User"

  # clone a package, all of its subscriptions
  def clone_to user
    cloned = Package.new(name: name, price: price, status: :active, specific_to: user)
    cloned.save!
    cloned.services << self.services
    cloned
  end

  SID = Struct.new(:inc, :id)
  def service_ids
    Service.all.map do |s|
      SID.new(services.include?(s), s.id)
    end
  end

  def service_ids= sids
    s = []
    sids.each do |sid, p|
      case p["inc"]
      when '1' then s << Service.find(sid)
      when '0' then nil
      else
        raise "invalid param inc"
      end
    end

    if new_record?
      @defered_saved_services = s
    else
      self.services = s
    end
  end

  def total_items_price
    services.map(&:price).reduce(0, &:+)
  end

  def savings
    1 - (self.price / self.total_items_price)
  end

private
  after_create do
    if @defered_saved_services.try(:any?)
      self.services = @defered_saved_services
      @defered_saved_services = nil
    end
  end
end
