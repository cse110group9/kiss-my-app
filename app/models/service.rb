class Service < ActiveRecord::Base
  validates_presence_of :price, :name, :description, :num_early_term_days
  validates_uniqueness_of :name
  validates_numericality_of :price, :num_early_term_days

  enum status: [:enabled, :disabled]
end
