class PackagedService < ActiveRecord::Base
  belongs_to :package
  belongs_to :service

  validates_uniqueness_of :package_id, scope: :service_id
end
