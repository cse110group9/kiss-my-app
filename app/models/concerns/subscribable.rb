module Subscribable
  extend ActiveSupport::Concern

  included do
    validates_presence_of :term_date
  end

  def term_length= term_str
    self.term_date = case term_str
    when "1week" then 1.week.from_now
    when "2weeks" then 2.weeks.from_now
    when "1month" then 1.month.from_now
    when "3months" then 3.months.from_now
    else 1.month.from_now
    end
  end
  def term_length
    "1month"
  end
end
