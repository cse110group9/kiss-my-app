class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :lockable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :name, :address, :email
  validates_uniqueness_of :email

  enum account_type: [:retail, :commercial, :marketing, :customer_service]
  validates_presence_of :account_type

  has_many :subscriptions
  has_many :services, through: :subscriptions, source: :subscribable, source_type: "Service"
  has_many :packages, through: :subscriptions, source: :subscribable, source_type: "Package"

  has_many :custom_packages,
    foreign_key: :specific_to_id,
    class_name: "Package"

  has_many :bills

  scope :customers, ->() { where(account_type: [
    User.account_types[:commercial],
    User.account_types[:retail]])
  }

  def generate_bill
    Bill.generate_for(self)
  end

  def total_debt
    bills.unpaid.map(&:cost).inject(0, &:+)
  end

  def account_type_string
    if self.account_type.to_s == "customer_service"
      "customer service"
    else
      account_type.to_s
    end
  end

private
  before_validation do
    if self.bill_threshold && self.bill_threshold < 0
      self.bill_threshold = nil
    end
    true
  end
end
