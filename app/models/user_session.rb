class UserSession
  cattr_accessor :current_ip
  def self.current_ip
    @@current_ip || "system"
  end
end
