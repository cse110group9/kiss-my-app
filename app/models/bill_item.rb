class BillItem < ActiveRecord::Base
  belongs_to :bill
  belongs_to :package_bill_item,  class_name: 'BillItem'
  has_many   :service_bill_items, class_name: 'BillItem', foreign_key: :package_bill_item_id

  enum item_type: [:package, :service, :early_term]

  scope :toplevel, ->() { where(package_bill_item: nil) }

  validates_presence_of :name, :price
end
