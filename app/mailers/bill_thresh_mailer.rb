class BillThreshMailer < ApplicationMailer
  def alert(user)
    @user = user
    @url = "http://localhost:3000/"
    mail(to: @user.email, subject: "Bill Cost Threshold Alert")
  end
end
