class ApplicationMailer < ActionMailer::Base
  default from: "kiss.my.app.noreply@gmail.com"
  layout 'mailer'
end
