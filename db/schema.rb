# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150311193613) do

  create_table "bill_items", force: :cascade do |t|
    t.integer  "item_type",                                     null: false
    t.decimal  "price",                precision: 4, scale: 10, null: false
    t.string   "name",                                          null: false
    t.integer  "package_bill_item_id"
    t.integer  "bill_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bills", force: :cascade do |t|
    t.datetime "paid_on"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "packaged_services", force: :cascade do |t|
    t.integer "service_id", null: false
    t.integer "package_id", null: false
  end

  create_table "packages", force: :cascade do |t|
    t.string   "name",                                                       null: false
    t.decimal  "price",               precision: 4, scale: 10
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "specific_to_id"
    t.integer  "status"
    t.decimal  "early_term_fee",      precision: 4, scale: 10, default: 0.0
    t.integer  "num_early_term_days",                          default: 5
  end

  create_table "services", force: :cascade do |t|
    t.string   "name",                                                       null: false
    t.decimal  "price",               precision: 4, scale: 10,               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
    t.integer  "status",                                       default: 0,   null: false
    t.decimal  "early_term_fee",      precision: 4, scale: 10, default: 0.0
    t.integer  "num_early_term_days",                          default: 5
  end

  create_table "subscription_rules", force: :cascade do |t|
    t.string   "rule_string"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "user_id",                                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "subscribable_id"
    t.string   "subscribable_type"
    t.datetime "term_date"
    t.integer  "num_early_term_days"
    t.decimal  "early_term_fee",      precision: 4, scale: 10
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                           default: "", null: false
    t.string   "encrypted_password",                              default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "address"
    t.integer  "failed_attempts",                                 default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.integer  "account_type",                                    default: 0,  null: false
    t.decimal  "bill_threshold",         precision: 4, scale: 10
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
