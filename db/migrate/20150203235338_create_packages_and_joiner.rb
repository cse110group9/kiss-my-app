class CreatePackagesAndJoiner < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      # name of the package, and its explicit price
      t.string :name, null: false
      t.decimal :price, scale: 10, precision: 2

      # nullable service, indicates this is a "transient" package
      t.references :service

      t.timestamps
    end

    create_table :package_users do |t|
      t.references :package
      t.references :user

      t.timestamps
    end
  end
end
