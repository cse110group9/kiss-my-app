class CreateSubscriptionRulesTable < ActiveRecord::Migration
  def change
    create_table :subscription_rules do |t|
      t.string :rule_string
      t.timestamps
    end
  end
end
