class AddStatusToServices < ActiveRecord::Migration
  def change
    add_column :services, :status, :integer, null: false, default: 0
  end
end
