class ChangePrecision < ActiveRecord::Migration
  def up
    change_column :bill_items, :price, :decimal, precision: 4, scale: 10
    change_column :packages, :price, :decimal, precision: 4, scale: 10
    change_column :services, :price, :decimal, precision: 4, scale: 10
    change_column :packages, :early_term_fee, :decimal, precision: 4, scale: 10
    change_column :services, :early_term_fee, :decimal, precision: 4, scale: 10
    change_column :users, :bill_threshold, :decimal, precision: 4, scale: 10
  end
  def down
    change_column :bill_items, :price, :decimal, precision: 2, scale: 10
    change_column :packages, :price, :decimal, precision: 2, scale: 10
    change_column :services, :price, :decimal, precision: 2, scale: 10
    change_column :packages, :early_term_fee, :decimal, precision: 2, scale: 10
    change_column :services, :early_term_fee, :decimal, precision: 2, scale: 10
    change_column :users, :bill_threshold, :decimal, precision: 2, scale: 10
  end
end
