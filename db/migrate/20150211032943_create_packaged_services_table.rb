class CreatePackagedServicesTable < ActiveRecord::Migration
  def change
    create_table :packaged_services do |t|
      t.references :service, null: false
      t.references :package, null: false
    end
  end
end
