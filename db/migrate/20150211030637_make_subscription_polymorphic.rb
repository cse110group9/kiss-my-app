class MakeSubscriptionPolymorphic < ActiveRecord::Migration
  def change
    remove_column :subscriptions, :service_id
    remove_column :subscriptions, :package_id
    add_column :subscriptions, :subscribable_id,   :integer
    add_column :subscriptions, :subscribable_type, :string
  end
end
