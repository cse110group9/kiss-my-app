class AddNumEarlyTermDays < ActiveRecord::Migration
  def change
    add_column :services, :num_early_term_days, :integer, default: 5
    add_column :packages, :num_early_term_days, :integer, default: 5
  end
end
