class MoveToServicesJoinTable < ActiveRecord::Migration
  def change
    drop_table :package_users
    drop_table :packaged_services
    remove_column :packages, :service_id

    create_table :subscriptions do |t|
      t.references :service, null: false
      t.references :user,    null: false
      t.references :package, default: nil

      t.timestamps
    end
  end
end
