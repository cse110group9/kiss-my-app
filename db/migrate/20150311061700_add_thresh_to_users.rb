class AddThreshToUsers < ActiveRecord::Migration
  def change
    add_column :users, :bill_threshold, :decimal, precision: 2, scale: 10
  end
end
