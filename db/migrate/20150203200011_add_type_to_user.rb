class AddTypeToUser < ActiveRecord::Migration
  def change
    change_table(:users) do |t|
      t.integer :account_type, null: false, default: 0
    end
  end
end
