class AddNumEarlyTermDaysToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :num_early_term_days, :integer, default: nil
    add_column :subscriptions, :early_term_fee, :decimal, precision: 4, scale: 10, default: nil
  end
end
