class AddStatusAndSpecificToPackage < ActiveRecord::Migration
  def change
    change_table :packages do |t|
      # is this a package specific to a certain user (cloned from a template package)?
      t.references :specific_to

      # status of the package: active, or discontinued
      t.integer :status
    end
  end
end
