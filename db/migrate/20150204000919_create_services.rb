class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string  :name,  null: false
      t.decimal :price, null: false, scale: 10, precision: 2

      t.timestamps
    end

    create_table :packaged_services do |t|
      t.references :service
      t.references :package

      t.timestamps
    end
  end
end
