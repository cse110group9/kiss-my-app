class AddTerminationFee < ActiveRecord::Migration
  def change
    add_column :services, :early_term_fee, :decimal, precision: 2, scale: 10, default: 0.0
    add_column :packages, :early_term_fee, :decimal, precision: 2, scale: 10, default: 0.0
  end
end
