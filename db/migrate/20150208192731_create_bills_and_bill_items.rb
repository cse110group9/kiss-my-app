 class CreateBillsAndBillItems < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.datetime :paid_on
      t.references :user
      t.timestamps
    end
    create_table :bill_items do |t|
      t.integer :item_type, null: false
      t.decimal :price, null: false, scale: 10, precision: 2
      t.string :name, null: false
      t.references :package_bill_item
      t.references :bill
      t.timestamps
    end
  end
end
